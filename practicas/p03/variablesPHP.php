<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head> 
    <body>
    <?php
        #Desarrollo de ejercicio 1
        echo "<h2>Ejercicio 1</h2>";
        $_myvar = "<p>La variable \"\$_myvar\" es válida porque comienza con underscore.\n</p>";
        echo $_myvar;
        $_7var = "La variable \"\$_7var\" es válida porque comienza con underscore.\n";
        echo "<p> $_7var </p>";
        #myvar
        echo "<p>La variable \"myvar\" no es válida porque no tiene $ al inicio.\n</p>";
        $myvar = "La variable \"\$myvar\" es válida porque comienza con letras.\n";
        echo "<p> $myvar </p>";
        $var7 = "La variable \"\$var7\" porque comienza con letras y le sigue un número.\n";
        echo "<p> $var7 </p>";
        $_element1 = "La variable \"\$_element1\" es válida porque comienza con underscore y le siguen letras y números.\n";
        echo"<p>$_element1 </p>";
        #$house*5
        echo "<p>La variable \"\$house*5\" no es válida porque * no es un caracter del códgigo ASCII del 127-255.\n</p>";   
        #Se liberan variables
        unset($_myvar, $_7var, $myvar, $var7,$_element1 );
        echo "<hr>";

        #Desarrollo de ejercicio 2
        echo "<h2>Ejercicio 2</h2>";
        echo "<h3>a)</h3>";
        $a = "ManejadorSQL";
        $b = 'MySQL';
        $c = &$a;
        echo " <p>a = $a \n</p>";
        echo " <p>b = $b \n</p>";
        echo " <p>c = $c \n</p>";
        echo "<h3>b) y c)</h3>";
        $a = "PHP server";
        $b = &$a;
        echo " <p>a = $a </p>";
        echo " <p>b = $b </p>";
        echo " <p>c = $c </p>";
        echo "<h3>d)</h3>";
        echo "<div>En el segundo bloque de asignaciones se modifica la variable \"a\" y a su vez el valor de la variable \"b\" y \"c\"";
        echo "<br> ya que a las últimas dos variables se les asignó el valor por referencia de \"a\", por lo tanto, si cambia \"a\" también \"b\" y \"c\"</div>";
        echo "<hr>";
        #Se liberan variables
        unset($a, $b, $c);

        #Desarrollo de ejercicio 3
        error_reporting(0);
        echo "<h2>Ejercicio 3</h2>";
        $a = "PHP5";
        echo " <p>a = $a => <b> " .gettype($a)."</b></p>";
        echo " <p>z[] =";
        $z[] = &$a; 
        print_r($z);
        echo " => <b> " .gettype($z)."</b></p>";
        $b = " 5a version de PHP";
        echo " <p>b = $b => <b> " .gettype($b)."</b></p>";
        $c = $b*10;
        echo " <p>c = $c => <b> " .gettype($c)."</b></p>";
        $a .= $b;
        echo " <p>a = $a => <b> " .gettype($a)."</b></p>";
        $b *= $c;
        echo " <p>b = $b => <b> " .gettype($b)."</b></p>";
        $z[0] = "MySQL";
        echo " <p>z[] =";
        print_r($z);
        echo " => <b> " .gettype($z)."</b></p>";
        echo "<hr>";

        #Desarrollo de ejercicio 4
        echo "<h2>Ejercicio 4</h2>";
        echo "<p>Acceso a valores de las variables \"a\", \"b\", \"c\" y \"z\" a través de la matriz \$GLOBALS</p>";
        echo "<p>a = " .$GLOBALS['a']."</p>";
        echo "<p>b = " .$GLOBALS['b']."</p>";
        echo "<p>c = " .$GLOBALS['c']."</p>";
        echo "<p>z = " ;
        print_r($GLOBALS['z']);
        echo "</p>";
        unset($a, $b, $c, $z);
        echo "<hr>";

        #Desarrollo de ejercicio 5
        echo "<h2>Ejercicio 5</h2>";
        $a = "7 personas";
        $b = (integer) $a;
        $a = "9E3";
        $c = (double) $a;   
        echo "<p>a = " .$a."</p>";
        echo "<p>b = " .$b."</p>";
        echo "<p>c = " .$c."</p>";
        unset($a, $b, $c);  
        echo "<hr>";   

        #Desarrollo de ejercicio 6
        echo "<h2>Ejercicio 6</h2>";
        $a = "0"; $b = "TRUE"; $c = FALSE; $d = ($a OR $b); $e = ($a AND $c); $f = ($a XOR $b);
        echo "<p><b><i>Verificando valor booleano</i></b>...</p>";
        function esBool ($variable){
            if (is_bool($variable))
                echo "<p>La variable es booleana.</p>";
            else
                echo "<p>La variable no es booleana.</p>";
        }
        esBool($a);
        esBool($b);
        esBool($c);
        esBool($d);
        esBool($e);
        esBool($f);
        var_dump($a,$b,$c,$d,$e,$f);
        echo "<p><b><i>Empleando funcion var_export() para poder imprimir valores de \"c\" y \"e\" con echo</i></b>...</p>";
        $c = var_export($c,true);
        echo "<p>c = $c</p>";
        $e = var_export($e,true);
        echo "<p>e = $c</p>";
        echo "<hr>";

        #Desarrollo de ejercicio 7
        echo "<h2>Ejercicio 7</h2>";
        echo "<p>a) Versión de Apache y PHP: <b> ".$_SERVER['SERVER_SOFTWARE']."</b></p>";
        echo "<p>b) Navegador y Sistema Operativo: <b> ".$_SERVER['HTTP_USER_AGENT']."</b></p>";
        echo "<p>Nombre de servidor: <b>".$_SERVER['SERVER_NAME']."</b></p>";
        echo "<p>c) Idioma del navegador: <b>".$_SERVER['HTTP_ACCEPT_LANGUAGE']."</b></p>";
        echo "<hr>";
    ?>
    </body>
</html>
